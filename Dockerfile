FROM node:20-buster-slim

RUN apt-get update && apt-get install -y --no-install-recommends git-core ca-certificates

COPY package.json /usr/local/semantic-release/package.json
COPY .releaserc.yml /usr/local/semantic-release/.releaserc.yml

WORKDIR /usr/local/semantic-release
RUN npm install semantic-release @semantic-release/changelog @semantic-release/gitlab @semantic-release/git @semantic-release/npm --save-dev